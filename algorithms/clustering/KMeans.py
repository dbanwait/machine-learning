import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

from sklearn.cluster import KMeans

# import data set
dataset = pd.read_csv('resources/Mall_Customers.csv')
variables = dataset.iloc[:, [3, 4]].values

# use elbow method to find optimal number of methods
wcss = []
for i in range(1, 11):
    tuneModel = KMeans(n_clusters=i, init='k-means++', max_iter=300, n_init=10, random_state=0)
    tuneModel.fit(variables)
    wcss.append(tuneModel.inertia_)

plt.plot(range(1, 11), wcss)
plt.title("Elbow Method")
plt.xlabel("Number of clusters")
plt.ylabel("WCSS")
plt.show()

# Apply K-Means to the dataset
model = KMeans(n_clusters=5, init='k-means++', max_iter=300, n_init=10, random_state=0)
resultantClusters = model.fit_predict(variables)

# visualize
plt.scatter(variables[resultantClusters == 0, 0], variables[resultantClusters == 0, 1], s=100, c="red", label='careful')
plt.scatter(variables[resultantClusters == 1, 0], variables[resultantClusters == 1, 1], s=100, c="blue", label='standard')
plt.scatter(variables[resultantClusters == 2, 0], variables[resultantClusters == 2, 1], s=100, c="green", label='target')
plt.scatter(variables[resultantClusters == 3, 0], variables[resultantClusters == 3, 1], s=100, c="cyan", label='careless')
plt.scatter(variables[resultantClusters == 4, 0], variables[resultantClusters == 4, 1], s=100, c="magenta", label='sensible')
plt.scatter(model.cluster_centers_[:, 0], model.cluster_centers_[:, 1], s=100, c="yellow", label='centroids')
plt.title('Clusters of clients')
plt.xlabel('Annual Income K')
plt.ylabel('Spending score')
plt.legend()
plt.show()
