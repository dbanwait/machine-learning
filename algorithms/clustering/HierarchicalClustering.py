import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

import scipy.cluster.hierarchy as sch
from sklearn.cluster import AgglomerativeClustering

# import data set
dataset = pd.read_csv('resources/Mall_Customers.csv')
variables = dataset.iloc[:, 3:].values

# Use dendrogram to find optimal number of clusters
dendrogram = sch.dendrogram(sch.linkage(variables, method='ward'))
plt.title('dendrogram')
plt.xlabel('customers')
plt.ylabel('euclidean distances')

# Fit HC to dataset
model = AgglomerativeClustering(n_clusters=5, affinity='euclidean', linkage='ward')
resultantClusters = model.fit_predict(variables)

# visualize (2-dimensions only)
plt.clf()
plt.scatter(variables[resultantClusters == 0, 0], variables[resultantClusters == 0, 1], s=100, c="red", label='careful')
plt.scatter(variables[resultantClusters == 1, 0], variables[resultantClusters == 1, 1], s=100, c="blue", label='standard')
plt.scatter(variables[resultantClusters == 2, 0], variables[resultantClusters == 2, 1], s=100, c="green", label='target')
plt.scatter(variables[resultantClusters == 3, 0], variables[resultantClusters == 3, 1], s=100, c="cyan", label='careless')
plt.scatter(variables[resultantClusters == 4, 0], variables[resultantClusters == 4, 1], s=100, c="magenta", label='sensible')
plt.title('Clusters of clients')
plt.xlabel('Annual Income K')
plt.ylabel('Spending score')
plt.legend()
plt.show()
