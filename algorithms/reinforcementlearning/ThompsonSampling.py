import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import random
import math

"""
    Thompson Sampling - Multi-armed bandit problem - exploit distribution with highest mean.
    - Probabilistic algorithm
    - Accommodates delayed feedback (as each round is build against a probabilistic distribution)
    - Better empirical evidence than UCB

    Example - online advertising campaign - which version of advert has best click through rate

    Step 1 - Consider results to date
    @:param adRewardCount1: Nx1(i) - number of times the ad x was selected (i.e. reward 1) up to round i
    @:param adRewardCount0: Nx0(i) - number of times the ad x was not selected (i.e. reward 0) up to round  i

    Step 2 - For each ad x, take a random draw from the beta distribution 
    use maths (Bayesian Inference), see doco
    Beta Distribution: https://stats.stackexchange.com/questions/47771/what-is-the-intuition-behind-beta-distribution

    Step 3 - Select ad that has highest probability of return
    

"""

# import dataset (simulation data - which ads each user will click on - real world this is down on fly)
dataset = pd.read_csv('resources/Ads_CTR_Optimisation.csv')


# random selection (control baseline)
n = 10000
numberAds = 10
adsSelected = []
totalReward = 0
for i in range(0, n):
    ad = random.randrange(numberAds)
    adsSelected.append(ad)
    reward = dataset.values[i, ad]
    totalReward = totalReward + reward
print(totalReward)

# implement Thompson Sampling
# step 1 - initialize variables to track results
adRewardCount1 = [0] * numberAds
adRewardCount0 = [0] * numberAds
adsSelected = []
totalReward = 0

# Step 2 - calculate the max upper bound for all ads each iteration
for i in range(0, n):
    ad = 0
    maxRandomDraw = 0

    for x in range(0, numberAds):
        # Step 2 - generate random draw from beta distribution for each ad
        randomBeta = random.betavariate(adRewardCount1[x] + 1, adRewardCount0[x] + 1)

        # Step 3 - select ad with highest random draw
        if randomBeta > maxRandomDraw:
            maxRandomDraw = randomBeta
            ad = x

    adsSelected.append(ad)
    reward = dataset.values[i, ad]
    totalReward = totalReward + reward

    if reward == 1:
        adRewardCount1[ad] = adRewardCount1[ad] + 1
    else:
        adRewardCount0[ad] = adRewardCount0[ad] + 1

# visualize
plt.hist(adsSelected)
plt.title('histogram ad selection')
plt.xlabel('advert')
plt.ylabel('number of times run')
plt.show()

