import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import random
import math

"""
    Upper Confidence Bound (UCB) - Multi-armed bandit problem - exploit distribution with highest mean.
    - Deterministic algorithm
    - Requires update at every round, before performing next round (otherwise nothing changes)

    Example - online advertising campaign - which version of advert has best click through rate
    
    Step 1 - Consider results to date
    Nx(i) - number of times the ad x was selected up to round i
    Rx(i) - sum of rewards of the ad x up to round i
    
    Step 2 - Refine confidence interval and bounds
    use maths
    
    Step 3 - Select ad that has highest UCB and exploit it for this round.

"""

# import dataset (simulation data - which ads each user will click on - real world this is down on fly)
dataset = pd.read_csv('resources/Ads_CTR_Optimisation.csv')

# random selection (control baseline)
n = 10000
numberAds = 10
adsSelected = []
totalReward = 0
for i in range(0, n):
    ad = random.randrange(numberAds)
    adsSelected.append(ad)
    reward = dataset.values[i, ad]
    totalReward = totalReward + reward
print(totalReward)

# implement UCB
# step 1 - initialize variables to track results
adSelectionCount = [0] * numberAds
adRewardCount = [0] * numberAds
adsSelected = []
totalReward = 0

# Step 2 - calculate the max upper bound for all ads each iteration
for i in range(0, n):
    maxUpperBound = 0
    maxUpperBoundAd = 0

    for x in range(0, numberAds):
        # for each ad
        if adSelectionCount[x] > 0:
            averageReward = adRewardCount[x] / adSelectionCount[x]
            deltaX = math.sqrt(3 / 2 * math.log(i + 1) / adSelectionCount[x])
            upperBound = averageReward + deltaX
        else:
            # run each ad at least once to seed our model (set upperBound high so this ad gets selected)
            upperBound = 9e999

        # Step 3 - select ad with highest upper confidence bound
        if upperBound > maxUpperBound:
            maxUpperBound = upperBound
            maxUpperBoundAd = x

    adsSelected.append(maxUpperBoundAd)
    adSelectionCount[maxUpperBoundAd] = adSelectionCount[maxUpperBoundAd] + 1
    reward = dataset.values[i, maxUpperBoundAd]
    adRewardCount[maxUpperBoundAd] = adRewardCount[maxUpperBoundAd] + reward
    totalReward = totalReward + reward

# visualize
plt.hist(adsSelected)
plt.title('histogram ad selection')
plt.xlabel('advert')
plt.ylabel('number of times run')
plt.show()

