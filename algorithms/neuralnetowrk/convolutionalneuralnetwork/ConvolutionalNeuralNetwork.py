import keras
from keras.models import Sequential
from keras.layers import Conv2D
from keras.layers import MaxPooling2D
from keras.layers import Flatten
from keras.layers import Dense
from keras.preprocessing.image import ImageDataGenerator

"""
    Convolutional Neural Network (extends ANN) - primarily used for image and text pattern recognition (also Go)
    https://keras.io/
    Father = Yann LeCun

    https://ujjwalkarn.me/2016/08/11/intuitive-explanation-convnets/
    http://scs.ryerson.ca/~aharley/vis/conv/flat.html
    https://adeshpande3.github.io/adeshpande3.github.io/The-9-Deep-Learning-Papers-You-Need-To-Know-About.html

    Step 1 - Convolution
        - Convert image pixels to binary arrary (b/w ? 2d : 3d)
        - Create Convolution Layer (i.e. multiple feature maps)
            - Feature map created by passing feature detector (aka kernel, filter) over an image (e.g. instagram)
            - Build many feature maps using different filters
            - Apply ReLU (rectifier function) to increase non-linearity

    Step 2 - Max Pooling (i.e. down-sampling)
        - introduce textual and spacial invariance in images (e.g. picture taken at an angle, or flipped)
        - stride along sub-section of feature map, find max value, and build new matrix

    Step 3 - Flattening
        - Convert all down-sampled maps into a single vector to feed into an ANN

    Step 4 - Full Connection
        - Build a fully connected ANN, each epoch will also update feature detectors and rebuild feature maps

    Example - classify images (cats and dogs)
        - note for > 2 categories
            - e.g. http://machinelearningmastery.com/handwritten-digit-recognition-using-convolutional-neural-networks-python-keras/
            - output activation function = softmax (instead of sigmoid)
            - loss function = categorical_crossentropy (instead of binary_crossentropy)
    
"""


"""" Part 1 - Data Pre-processing """
# images need to be prepared into a folder structure split into train/test and categories

"""" Part 2 - Create CNN """
# create a sequential ANN
neuralNetwork = Sequential()

# Step 1 - Convolution Layer - create feature maps
neuralNetwork.add(Conv2D(filters=32, strides=1, kernel_size=(3, 3), input_shape=(64, 64, 3), activation='relu'))

# Step 2 - Pooling Layer - reduce size of feature maps, without losing information
neuralNetwork.add(MaxPooling2D(pool_size=(2, 2)))

# add a second convolution layer (try to improve accuracy)
neuralNetwork.add(Conv2D(filters=32, strides=1, kernel_size=(3, 3), activation='relu'))
neuralNetwork.add(MaxPooling2D(pool_size=(2, 2)))

# Step 3 - Flatten - take all feature maps and put into a massive single vector to feed ANN
neuralNetwork.add(Flatten())

# Step 4 - process flattened vector through a fully connected ANN
# add input layer (32 nodes) and first hidden layer (128 neurons/units)
hiddenLayer = Dense(units=128, activation='relu')
neuralNetwork.add(hiddenLayer)

# add output layer (note: if multi-category output then increase units and use 'softmax' activation
hiddenLayer = Dense(units=1, activation='sigmoid')
neuralNetwork.add(hiddenLayer)

# compile ANN ('adam' is stochastic gradient decent)
neuralNetwork.compile(optimizer='adam', loss='binary_crossentropy', metrics=['accuracy'])

# Image Augmentation - to increase data and randomize characteristics (e.g rotate images)
trainDataGen = ImageDataGenerator(
        rescale=1./255,
        shear_range=0.2,
        zoom_range=0.2,
        horizontal_flip=True)

testDataGen = ImageDataGenerator(rescale=1. / 255)

trainSet = trainDataGen.flow_from_directory(
        'resources/catsanddogs/dataset/training_set',
        target_size=(64, 64),
        batch_size=32,
        class_mode='binary')

testSet = testDataGen.flow_from_directory(
        'resources/catsanddogs/dataset/test_set',
        target_size=(64, 64),
        batch_size=32,
        class_mode='binary')

neuralNetwork.fit_generator(
        trainSet,
        steps_per_epoch=200,
        epochs=25,
        validation_data=testSet,
        validation_steps=100)

"""" Part 3 - Making predictions and evaluating the model """

