import pandas as pd
from sklearn.preprocessing import LabelEncoder, OneHotEncoder
from sklearn.model_selection import train_test_split as TrainTestSplit
from sklearn.metrics import confusion_matrix as confusionMatrix
from sklearn.preprocessing import StandardScaler
from sklearn.metrics import accuracy_score

import keras
from keras.models import Sequential
from keras.layers import Dense

""""
    Artificial Neural Network

    Libraries
        Theano
            - high performance numerical calculation library that can run on CPU or GPU
            - pip install --upgrade --no-deps git+git://github.com/Theano/Theano.git
        TensorFlow
            - high performance numerical calculation library that can run on CPU or GPU
            - https://www.tensorflow.org/install/install_windows#installing_with_anaconda
        Keras (https://keras.io/)
            - wraps Theono & TensorFlow libraries, to build Deep Learning models efficiently with few lines of code
            - pip install --upgrade keras
            
    Neuron Activation Functions
        - rectifier (generally best for hidden layers)
        - sigmoid (good for output layer, to get probabilities)
            
    Example - Classification problem
        - model the probability of a customer to leave a bank based on various attributes

"""


"""" Part 1 - Data Pre-processing """
# import data set
dataset = pd.read_csv('resources/Churn_Modelling.csv')
iVariables = dataset.iloc[:, 3:13].values
dVariables = dataset.iloc[:, 13].values

# Categorical Variables - encode to int value
colIdx = 1
iLabelEncoder = LabelEncoder()
iVariables[:, colIdx] = iLabelEncoder.fit_transform(iVariables[:, colIdx])
colIdx = 2
iLabelEncoder = LabelEncoder()
iVariables[:, colIdx] = iLabelEncoder.fit_transform(iVariables[:, colIdx])

# Categorical Variables - remove dummy variable trap
colIdx = 1
iOneHotEncoder = OneHotEncoder(categorical_features=[colIdx])
iVariables = iOneHotEncoder.fit_transform(iVariables).toarray()
iVariables = iVariables[:, 1:]

# split into training and test sets (80:20)
iTrain, iTest, dTrain, dTest = TrainTestSplit(iVariables, dVariables, test_size=0.2, random_state=0)

# remove feature bias (i.e. feature scaling)
iScalar = StandardScaler()
iTrain = iScalar.fit_transform(iTrain)
iTest = iScalar.transform(iTest)

"""" Part 2 - Create ANN """
# create a sequential ANN
neuralNetwork = Sequential()

# Step 1 - Create layers
# add input layer (11 nodes) and first hidden layer (6 neurons/units)
hiddenLayer = Dense(units=6, activation='relu', kernel_initializer='uniform', input_dim=11)
neuralNetwork.add(hiddenLayer)

# add second hidden layer (no longer need to add input_dim)
hiddenLayer = Dense(units=6, activation='relu', kernel_initializer='uniform')
neuralNetwork.add(hiddenLayer)

# add output layer (note: if multi-category output then increase units and use 'softmax' activation
hiddenLayer = Dense(units=1, activation='sigmoid', kernel_initializer='uniform')
neuralNetwork.add(hiddenLayer)

# compile ANN ('adam' is stochastic gradient decent)
# Loss function = use 'categorical_crossentropy' loss function for multi category output
neuralNetwork.compile(optimizer='adam', loss='binary_crossentropy', metrics=['accuracy'])

# Step 2 - Input first observation into input layer, one independent variable per input node
# Step 3 - Forward Activation neurons are activated such that the impact of each neuron activation is limited by weights
# Step 4 - Error - calculate the error between predicted output and observed output
# Step 5 - Back-Propagation; feed back error to ANN and update the weights according to how much they are responsible
# Step 6 - Repeat 1-5 - (Batch Learning = updated weights after batch) vs (Reinforcement Learning = update every obs)
# Step 7 - Epoch - an Epoch is complete, repeat and do more epoch.
neuralNetwork.fit(iTrain, dTrain, batch_size=10, epochs=100)


"""" Part 3 - Making predictions and evaluating the model """
dPrediction = neuralNetwork.predict(iTest)
# convert probability based output into binary output
dPrediction = (dPrediction > 0.5)

# check accuracy
conMatrix = confusionMatrix(dTest, dPrediction)
accuracy = accuracy_score(dTest, dPrediction)

