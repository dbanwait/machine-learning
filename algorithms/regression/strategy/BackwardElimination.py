import numpy as np
import statsmodels.formula.api as sm


def prependConstantColumn(X):
    """

    :param X: Independent variable matrix (i.e. X)
    :return: X with a first column added with constant = 1
    """
    rows = X.shape[0]
    constant_column = np.ones(shape=(rows, 1)).astype(int)
    X = np.append(arr=constant_column, values=X, axis=1)
    return X


def generateOLSModel(X_all, y):
    """

    :param X_all: All independent variables we want to build this model against (including constant column)
    :param y: Expected set of outputs
    :return: Ordinary Least Squares model fitted to X_all for best fit against X_all
    """
    return sm.OLS(endog=y, exog=X_all).fit()


def optimizeModel(model, X, X_features, y, sl):
    """


    :param model: Ordinary Least Squared model already fitted to X
    :param X: All independent variables model was built against (including constant column)
    :param X_features: List of features used by the model (which have not yet been dropped)
    :param y: Expected set of outputs
    :param sl: Significant Level for elimination. P-value > SL will be eliminated. Generally set to 0.05 (5%)
    :return: optimized model that has been through backward elimination
    """

    maxAdjR2 = model.rsquared_adj
    while model.pvalues.max() > sl:
        print(model.summary())
        print("Model has P-value {} > {} significance level. Starting backward elimination.", model.pvalues.max(), sl)
        p_max_idx = np.where(model.pvalues == model.pvalues.max())
        X_features.pop(p_max_idx[0])
        print("Removed feature with max p-value. New optimal feature set is {}", X_features)
        newModel = sm.OLS(endog=y, exog=X[:, X_features]).fit()

        if maxAdjR2 > newModel.rsquared_adj:
            print(
                "New model has a lower adjusted-R2. Using previous model. " + newModel.rsquared_adj + " vs " + maxAdjR2)
            return model

        model = newModel

    print("Optimal model found with features {}", X_features)
    model.summary()

    return model
