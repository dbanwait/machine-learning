import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

from sklearn.ensemble import RandomForestRegressor

# import data set
dataset = pd.read_csv('resources/02_6_Position_Salaries.csv')
iVariables = dataset.iloc[:, 1:2].values
dVariables = dataset.iloc[:, 2].values

# split into training and test sets (80:20)
# iTrain, iTest, yTrain, yTest = TrainTestFit(iVariables, dVariables, test_size=1/3, random_state=0)

# remove feature bias (i.e. feature scaling)
# n/a: applied by LinearRegression library already


# build model
model = RandomForestRegressor(n_estimators=300, criterion='mse', random_state=0)
model.fit(iVariables, dVariables)

# check predictions (manually)
dPrediction = model.predict(6.5)

# visualize model (2-dimensions)
xGrid = np.arange(min(iVariables), max(iVariables), 0.01)
xGrid = xGrid.reshape((len(xGrid)), 1)
plt.scatter(iVariables, dVariables, color='red')
plt.plot(xGrid, model.predict(xGrid), color='blue')
plt.title("Random Forest Model")
plt.xlabel("Position Level")
plt.ylabel("Salary")
plt.show()
