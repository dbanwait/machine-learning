import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from sklearn.cross_validation import train_test_split as TrainTestFit
from sklearn.linear_model import LinearRegression
from sklearn.preprocessing import PolynomialFeatures

import algorithms.visualize.Plot as visualize

""" Import the data set
    X: independent variables (features) (should be matrix)
    y: dependant variable (result) (should be vector)"""
dataset = pd.read_csv('resources/02_6_Position_Salaries.csv')
iVariables = dataset.iloc[:, 1:2].values
dVariables = dataset.iloc[:, 2].values

""" Training (80%) & Test (20%) dataset split """
# iTrain, iTest, yTrain, yTest = TrainTestFit(iVariables, dVariables, test_size=1/3, random_state=0)

""" Feature scaling """
# n/a: applied by LinearRegression library already


""" Create & Fit Regression model to the dataset """
model

""" Predict result from Model """
dPrediction = model.predict(6.5)

""" Visualize the Regression Model """
plt.scatter(iVariables, dVariables, color='red')
plt.plot(iVariables, model.predict(iVariables), color='blue')
plt.title("Regression Model")
plt.xlabel("x-axis")
plt.ylabel("y-axis")
plt.show()

""" Visualize the Regression Model (higher resolution) """
xGrid = np.arange(min(iVariables), max(iVariables), 0.1)
xGrid = xGrid.reshape((len(xGrid)), 1)
plt.scatter(iVariables, dVariables, color='red')
plt.plot(xGrid, model.predict(xGrid), color='blue')
plt.title("Regression Model")
plt.xlabel("x-axis")
plt.ylabel("y-axis")
plt.show()
