import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from sklearn.svm import SVR
from sklearn.preprocessing import StandardScaler

# import data set
dataset = pd.read_csv('resources/02_6_Position_Salaries.csv')
iVariables = dataset.iloc[:, 1:2].values
dVariables = dataset.iloc[:, 2].values

# split into training and test sets (80:20)
# iTrain, iTest, yTrain, yTest = TrainTestFit(iVariables, dVariables, test_size=1/3, random_state=0)

# remove feature bias (i.e. feature scaling)
# SVR library does not do this for free
iScalar = StandardScaler()
iVariables = iScalar.fit_transform(iVariables)
dScalar = StandardScaler()
dVariables = dScalar.fit_transform(dVariables)

# build model
model = SVR(kernel='rbf')
model.fit(iVariables, dVariables)

# check predictions (manually)
dPrediction = model.predict(iScalar.transform(np.array([[6.5]])))
result = dScalar.inverse_transform(dPrediction)

# visualize model
xGrid = np.arange(min(iVariables), max(iVariables), 0.1)
xGrid = xGrid.reshape((len(xGrid)), 1)
plt.scatter(iVariables, dVariables, color='red')
plt.plot(xGrid, model.predict(xGrid), color='blue')
plt.title("SVR Model")
plt.xlabel("Position Level")
plt.ylabel("Salary")
plt.show()
