import pandas as pd
from sklearn.cross_validation import train_test_split as TrainTestSplit
from sklearn.metrics import confusion_matrix as confusionMatrix
from sklearn.neighbors import KNeighborsClassifier
from sklearn.preprocessing import StandardScaler

import algorithms.visualize.Plot as visualize

# import data set
dataset = pd.read_csv('resources/Social_Network_Ads.csv')
iVariables = dataset.iloc[:, [2, 3]].values
dVariables = dataset.iloc[:, 4].values

# split into training and test sets (80:20)
iTrain, iTest, dTrain, dTest = TrainTestSplit(iVariables, dVariables, test_size=0.25, random_state=0)

# remove feature bias (i.e. feature scaling)
iScalar = StandardScaler()
iTrain = iScalar.fit_transform(iTrain)
iTest = iScalar.transform(iTest)

# build model
model = KNeighborsClassifier(n_neighbors=5, metric='minkowski', p=2)
model.fit(iTrain, dTrain)

# check predictions (manually)
dPrediction = model.predict(iTest)
deltaPrediction = dTest - dPrediction

# check predictions (confusion matrix)
conMatrix = confusionMatrix(dTest, dPrediction)

# visualize model (2-dimensions)
visualize.plotClassification2D(model, iTrain, dTrain, title='Training Set', xLabel='Age', yLabel='Salary')
visualize.plotClassification2D(model, iTest, dTest, title='Training Set', xLabel='Age', yLabel='Salary')